= Kaamebott
Jean-Louis Jouannic <jeanlouis.jouannic@gmail.com>
:toc:
:toc-title: Table des matières
:icons: font
:experimental:

Un simple _bot_ Slack pour enrichir les conversations Slack d'extraits audios
de Kaamelott issus de la formidable
link:https://github.com/2ec0b4/kaamelott-soundboard[kaamelott-soundboard, target=_blank].

== Développement

image::ca-doit-etre-du-code.png["Ça, ça doit être du code parce que ça veut rien dire.",300]

=== Pré-requis

* Node 16.x au minimum,
* créer une application Slack,
* un accès administrateur sur un espace de travail Slack.

=== Récupérer le code

Ce projet référence le projet
link:https://github.com/2ec0b4/kaamelott-soundboard[kaamelott-soundboard, target=_blank]
dans un sous-module Git, penser donc à l'option `--recursive` au moment de
cloner le projet.

```console
$ git clone --recursive https://gitlab.com/jljouannic/kaamebott.git
```

=== Tunnel vers l'hôte local

Durant le développement il faut démarrer un tunnel qui permet d'accéder depuis
le Web au _bot_ qui fonctionne en local sur le poste de développement.
Pour cela j'utilise
link:https://github.com/localtunnel/localtunnel[localtunnel] :

```console
$ npx localtunnel --port 3000
npx : 35 installé(s) en 1.896s
your url is: https://funny-robin-39.loca.lt
```

=== Configuration de l'application Slack

La configuration de l'application Slack se fait depuis le
link:https://api.slack.com/apps[site dédié^] et nécessite d'être administrateur
de l'espace de travail Slack depuis lequel le _bot_ sera interrogé.

. Dans la section menu:Feature[OAuth & Permissions > Scopes > Bot Token Scopes],
ajouter les scopes suivants, nécessaires au fonctionnement du _bot_ :
** `commands` pour activer le support des _slash commands_,
** `chat:write` pour autoriser le _bot_ à ajouter des messages,
** `files.write` pour autoriser le _bot_ à téléverser des fichier (les extraits
audio).
. Dans la section
menu:Feature[OAuth & Permissions > Scopes > OAuth Tokens & Redirect URLs],
cliquer sur le bouton btn:[Install to Workspace].
. Dans la section menu:Feature[Slash Commands], configurer une commande pour
interroger le _bot_ depuis Slack. Dans le champ `Request URL`, indiquer
l'URL suivante de l'API du _bot_ : `\https://xxxxx.xxxxx.xxx/api/messages`.
. Dans la section menu:Feature[Interactivity & Shortcuts], activer le support de
l'interactivité puis indiquer à nouveau l'URL de l'étape précédente dans le
champ `Request URL` qui apparaît.

=== Variables d'environnement

Les variables d'environnement suivantes doivent être définies :

`CLIENT_SIGNING_SECRET`:: Dans la configuration de l'application, section
menu:Settings[Basic Information > App Credentials], copier la valeur du champ
`Signing Secret`.
`BOT_TOKEN`:: Toujours dans la configuration de l'application, la valeur de
cette variable peut être copié depuis la section
menu:Features[OAuth & Permissions > OAuth Tokens for Your Team] dans le champ
`Bot User OAuth Access Token`.
+
NOTE: Ce token est disponible uniquement après que l'application a été installée
dans un espace de travail Slack.

== Utilisation

L'utilisateur interragit avec le _bot_ au travers d'une _slash command_.
Dans la configuration de l'application Slack, j'ai choisi de nommer ma commande
`/kaam`.
Suivant comment elle est utilisée, le bot interprète la commande de différentes
manières :

* si elle est utilisée seule, le _bot_ sélectionne au hasard un extrait audio
et le téléverse sur Slack, dans le _channel_ depuis lequel la _slash command_
a été émise.
* Si elle est suivi d'un texte, celui-ci est interprété dans un premier temps
comme étant un nom de personnage.
** Si un personnage portant ce nom est trouvé, le _bot_ téléverse un extrait
audio au hasard de ce personnage.
+
.Extrait sonore au hasard d'un personnage
[example]
```
/kaam burgonde
```
** Sinon, le texte est utilisé pour une recherche dans la retranscription des
extraits audios. Ensuite, trois possibilités :
*** Si un seul extrait correspond, le _bot_ le téléverse dans le _channel_
Slack.
+
.Recherche _full text_ (résultat unique)
[example]
```
/kaam graine
```
*** Si plusieurs extraits sont trouvés, les 5 premiers sont proposés à
l'utilisateur dans le _channel_ Slack pour qu'il fasse son choix.
+
.Recherche _full text_ (plusieurs résultats)
[example]
```
/kaam nul
```
*** Si aucun extrait n'est trouvé, le _bot_ en informe l'utilisateur.
+
.Recherche sans résultat
[example]
```
/kaam aucune chance de trouver un extrait qui correspond
```
