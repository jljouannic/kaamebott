import fs from 'fs'
import botkit from 'botkit'
import slackAdapter, { SlackMessageTypeMiddleware } from 'botbuilder-adapter-slack'
import dotenv from 'dotenv'
import { getSound, randomWithCharacter, searchSounds } from './services/index.js'

const { createReadStream } = fs
const { Botkit } = botkit
const { SlackAdapter } = slackAdapter

dotenv.config()

const adapter = new SlackAdapter({
  clientSigningSecret: process.env.CLIENT_SIGNING_SECRET,
  botToken: process.env.BOT_TOKEN,
})
adapter.use(new SlackMessageTypeMiddleware()) // mandatory to detect block_actions

const controller = new Botkit({
  webhook_uri: '/api/messages',
  adapter,
})

const formatTitle = (characters, title) => `${characters.join(' et ')} — « ${title} »`

const uploadSound = ({ characters, title, file }, bot, message) =>
  bot.api.files
    .upload({
      title: formatTitle(characters, title),
      channels: message.channel,
      file: createReadStream(`./soundboard/sounds/${file}`),
    })
    .catch((reason) => {
      if (reason?.data?.error === 'not_in_channel') {
        return bot.replyPrivate(message, {
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `Pour que ça fonctionne, il faut m'inviter dans le channel : \`/invite <@${message.reference.bot.id}>\``,
              },
            },
          ],
          channel: message.channel,
        })
      }
      return Promise.reject(reason)
    })

const noResult = (bot, message) =>
  bot.replyPrivate(message, {
    text: "Non, moi j'crois qu'il faut qu'vous arrêtiez d'essayer de dire des trucs. Ça vous fatigue, déjà, et pour les autres, vous vous rendez pas compte de c'que c'est. Moi quand vous faites ça, ça me fout une angoisse... j'pourrais vous tuer, j'crois. De chagrin, hein ! J'vous jure c'est pas bien. Il faut plus que vous parliez avec des gens.",
    channel: message.channel,
  })

controller.on('slash_command', (bot, message) => {
  const { channel, text } = message

  // 1. consider text as a character name (which may be empty)
  const sound = randomWithCharacter(text)
  if (sound) {
    return uploadSound(sound, bot, message)
  }

  // 2. use text as search keywords
  const sounds = searchSounds(text)
  if (sounds.length === 1) {
    // a. unique result
    return uploadSound(sounds[0], bot, message)
  }
  if (sounds.length) {
    // b. many results
    return bot.replyPrivate(message, {
      blocks: sounds
        .slice(0, 5) // keep only the first results
        .reduce((acc, { id, title, characters }, index) => {
          if (index) {
            acc.push({ type: 'divider' })
          }
          acc.push({
            type: 'section',
            text: {
              type: 'plain_text',
              text: formatTitle(characters, title),
              emoji: false,
            },
            accessory: {
              type: 'button',
              text: {
                type: 'plain_text',
                text: '▶',
                emoji: false,
              },
              value: id,
              action_id: 'play-sound',
            },
          })
          return acc
        }, []),
      channel,
    })
  }

  // c. no result
  return noResult(bot, message)
})

controller.on('block_actions', (bot, message) => {
  const sound = getSound(message.text)
  if (sound) {
    return uploadSound(sound, bot, message)
  }
  return noResult(bot, message)
})
