import ASCIIFolder from 'fold-to-ascii'
import { soundsByCharacters } from './sounds.js'

export const characters = Object.keys(soundsByCharacters).sort()

const canonicalize = (string) => ASCIIFolder.foldReplacing(string).toLowerCase()

const canonicalToCharacters = characters.reduce((acc, character) => {
  acc[canonicalize(character)] = character
  return acc
}, {})

const canonicalCharacters = Object.keys(canonicalToCharacters)

export const findCharacter = (name) => {
  const canonicalName = canonicalize(name)
  return canonicalToCharacters?.[canonicalCharacters.find((c) => c.includes(canonicalName))] ?? null
}
