import { findCharacter } from './characters.js'

describe('findCharacter', () => {
  test('should return null if character is unknown', () => {
    expect(findCharacter('Astérix')).toBeNull()
  })
  ;[
    ['tintagel', 'Ygerne de Tintagel'],
    ['arthur', 'Arthur'],
    ["Elias de Kelliwic'h", "Elias de Kelliwic'h"],
  ].forEach(([param, expected]) => {
    test(`with param "${param}" should find "${expected}"`, () => {
      expect(findCharacter(param)).toBe(expected)
    })
  })
})
