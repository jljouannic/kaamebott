import { v5 as uuid } from 'uuid'
// eslint-disable-next-line import/no-relative-packages
import json from '../../soundboard/sounds/sounds.json'

// it has strictly no meaning at all, simply generated in a shell with `uuidgen`
const namespace = '8162e508-24fa-4565-9779-30bfd96e25d1'

// for each extract, transform character string to an array of characters (e.g "Arthur - Le roi Burgonde" => ["Arthur", "Le roi Burgonde"])
export default json.map(({ character, file, ...rest }) => ({
  id: uuid(file, namespace),
  characters: character.split(/\s*-\s*/),
  file,
  ...rest,
}))
