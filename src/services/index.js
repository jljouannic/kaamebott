import { findCharacter } from './characters.js'
import { sounds, soundsByCharacters } from './sounds.js'

const randomFrom = (array) => array[Math.floor(Math.random() * array.length)]

export const randomWithCharacter = (name) => {
  if (name) {
    const character = findCharacter(name)
    return character && randomFrom(soundsByCharacters[character])
  }
  return randomFrom(sounds)
}

export { getSound, searchSounds } from './sounds.js'
