import { randomWithCharacter } from './index.js'
import { sounds, soundsByCharacters } from './sounds.js'

describe('randomSound', () => {
  test('should return null if character is unknown', () => {
    expect(randomWithCharacter('Astérix')).toBeNull()
  })

  test('should return a random sound', () => {
    const sound = randomWithCharacter()
    expect(sound).not.toBeNull()
    expect(sounds).toContain(sound)
  })
  ;[
    ['seli', 'Séli'],
    ['Elias', "Elias de Kelliwic'h"],
  ].forEach(([param, character]) => {
    test(`with param "${param}" should return a random sound of ${character}`, () => {
      const sound = randomWithCharacter(param)
      expect(sound).not.toBeNull()
      expect(soundsByCharacters[character]).toContain(sound)
    })
  })
})
