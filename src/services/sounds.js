import data from './data.js'

export const sounds = data

const soundsById = data.reduce((acc, sound) => {
  acc[sound.id] = sound
  return acc
}, {})

// index each sound by its character name
export const soundsByCharacters = data.reduce((acc, sound) => {
  const { characters } = sound
  characters.forEach((character) => {
    if (acc[character]) {
      acc[character].push(sound)
    } else {
      acc[character] = [sound]
    }
  })
  return acc
}, {})

export const searchSounds = (keywords) => {
  if (!keywords) {
    return []
  }
  const canonical = keywords.toLowerCase()
  return data.filter(({ title }) => title.toLowerCase().includes(canonical))
}

export const getSound = (id) => soundsById[id]
