import { searchSounds } from './sounds.js'

describe('searchSounds', () => {
  test('should return [] if called with no parameter', () => {
    expect(searchSounds().length).toBe(0)
  })
  test('should return [] if no matching sound is found', () => {
    expect(searchSounds('ljkfvbgsdlkfjbvksdfjbvkljdlkbnsf').length).toBe(0)
  })
  ;[
    ['faux', 2],
    ['graine', 1],
    ['poulette', 2],
  ].forEach(([keywords, resultCount]) => {
    test(`should return ${resultCount} result(s) matching "${keywords}"`, () => {
      expect(searchSounds(keywords).length).toBe(resultCount)
    })
  })
})
